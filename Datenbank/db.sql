# CREATE DATABASE `kotlingruppe`;

USE kotlingruppe;

CREATE TABLE `t_person` (
`gastname` VARCHAR(255) NOT NULL UNIQUE,
`rolle` BOOLEAN
);

CREATE TABLE `t_musiktitel`(
`p_musiktitel_id` INT NOT NULL AUTO_INCREMENT,
`titelname` VARCHAR(255),
`bandname` VARCHAR(255),
`genre` VARCHAR(255),
CONSTRAINT `pk_t_musiktitel` PRIMARY KEY (`p_musiktitel_id`),
UNIQUE (`titelname`, `bandname`)
);

CREATE TABLE `t_person_musiktitel` (
`p_person_musiktitel_id` INT NOT NULL AUTO_INCREMENT,
`f_gastname` VARCHAR(255) NOT NULL,
`f_musiktitel_id` INT NOT NULL,
CONSTRAINT `pk_person_musiktitel` PRIMARY KEY (`p_person_musiktitel_id`),
CONSTRAINT `t_personen_musiktitel_id` FOREIGN KEY (`f_musiktitel_id`) REFERENCES t_musiktitel (`p_musiktitel_id`)
    ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT `t_personen_musiktitel` FOREIGN KEY (`f_gastname`) REFERENCES t_person (`gastname`)
	ON DELETE CASCADE ON UPDATE CASCADE
);

-- CREATE USER 'musikvoter'@'localhost' IDENTIFIED BY '123';
-- GRANT SELECT, INSERT ON kotlingruppe.* TO 'musikvoter'@'localhost';

CREATE USER 'musikvoter'@'%' IDENTIFIED BY '123';
GRANT SELECT, INSERT, DELETE ON kotlingruppe.* TO 'musikvoter'@'%';

FLUSH PRIVILEGES;