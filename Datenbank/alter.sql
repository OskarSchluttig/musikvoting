ALTER TABLE t_person
    ADD adelsgeschlecht varchar(256);

DELETE FROM t_person_musiktitel; # Da diese Änderung zwischen zwei Partys stattfindet, kann die Tabelle also getrost gelöscht werden... Hoffentlich

ALTER TABLE t_person_musiktitel
    ADD p_votetime TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
    DROP PRIMARY KEY,
    ADD PRIMARY KEY(p_votetime),
    DROP COLUMN p_person_musiktitel_id;
