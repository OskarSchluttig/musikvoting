# Register Funktion
REPLACE INTO t_person VALUES (?, ?, ?);

# Login-Query User Rolle abzufragen
SELECT rolle FROM t_person WHERE gastname = ?;

# Anzeigen aller Songs
SELECT p_musiktitel_id, titelname, bandname, genre FROM t_musiktitel;

#Funktion zum Eintragen eines Musiktitels
INSERT INTO `t_musiktitel` (`titelname`, `bandname`, `genre`) VALUES(?,?,?);

#Funktion zum Eintragen eines Musikwunsches 
INSERT INTO t_person_musiktitel(f_gastname, f_musiktitel_id) VALUES (?, ?);

#Funktion von einer Person (5 letzten, aktuellen) Musikwünsche erfragen
SELECT t1.f_musiktitel_id, COUNT(t1.f_musiktitel_id) AS anzahl
    FROM (SELECT t2.f_musiktitel_id FROM t_person_musiktitel AS t2
    WHERE `f_gastname` = ?  ORDER By p_votetime DESC  LIMIT 5) AS t1
    GROUP BY t1.f_musiktitel_id;

#Funktion zum Abfragen aller Musikwünsche (Playlist) vom Gastgeber (hier fehlt noch, dass die Anzahl der eingehenden Votes ja nicht größer als 5 sein darf)
SELECT allSongs.*, COUNT(*) AS Anzahl
    FROM (SELECT allVotes1.f_musiktitel_id FROM t_person_musiktitel AS allVotes1
            LEFT JOIN t_person AS allPersons ON allVotes1.f_gastname = allPersons.gastname
            WHERE allPersons.adelsgeschlecht IS NOT NULL
            AND (SELECT COUNT(*) FROM  t_person_musiktitel AS allVotes2
				WHERE allVotes2.f_gastname = allVotes1.f_gastname
				    AND allVotes2.p_votetime >= allVotes1.p_votetime) <= 5) AS allValidVotes
    LEFT JOIN t_musiktitel AS allSongs ON allValidVotes.f_musiktitel_id = allSongs.p_musiktitel_id
    GROUP BY allValidVotes.f_musiktitel_id
    ORDER BY Anzahl DESC;
