INSERT INTO `t_person` (`gastname`, `rolle`) VALUES ("Peter" , true);
INSERT INTO `t_person` (`gastname`, `rolle`) VALUES ("Luise" , false);
INSERT INTO `t_person` (`gastname`, `rolle`) VALUES ("Henrike" , false);
INSERT INTO `t_person` (`gastname`, `rolle`) VALUES ("Paul" , false);
INSERT INTO `t_person` (`gastname`, `rolle`) VALUES ("Hans" , false);
INSERT INTO `t_person` (`gastname`, `rolle`) VALUES ("Marie" , false);

INSERT INTO `t_musiktitel`(`titelname`,`bandname`,`genre`) VALUES ("We will Rock you", "Queen", "Rock");
INSERT INTO `t_musiktitel`(`titelname`,`bandname`,`genre`) VALUES ("Die kleine Nachtmusik", "Mozart", "Klassik");
INSERT INTO `t_musiktitel`(`titelname`,`bandname`,`genre`) VALUES ("Girls just want to have fun", "Cyndi Lauper", "Pop");
INSERT INTO `t_musiktitel`(`titelname`,`bandname`,`genre`) VALUES ("Lose Yourself", "Eminem", "Hip Hop");
INSERT INTO `t_musiktitel`(`titelname`,`bandname`,`genre`) VALUES ("5.Symphony", "Beethoven", "Klassik");
INSERT INTO `t_musiktitel`(`titelname`,`bandname`,`genre`) VALUES ("Born to be wild", "Steppenwolf", "Rock");

# INSERT INTO `t_person_musiktitel`(`f_gastname`, `f_musiktitel_id`) VALUES ("Luise", 1);