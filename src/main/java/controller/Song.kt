package controller

data class Song (val id: Int, val title: String, val band: String, val genre: String, var votes: Int = 0)