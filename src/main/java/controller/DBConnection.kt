package controller

import java.sql.Connection
import java.sql.DriverManager
import java.sql.Statement
import javax.naming.AuthenticationException
import java.sql.SQLException
import java.sql.SQLIntegrityConstraintViolationException


class DBConnection(private val ipAdresse: String,
                   private val datenbank: String,
                   private val dbUsername: String,
                   private val dbPassword: String) {

    private lateinit var con: Connection
    var user: String = ""
        private set
    var isHost = false
        private set
    lateinit var allSongs: ArrayList<Song>
        private set


    private fun getConnection(): Connection {

        val url = "jdbc:mysql://${ipAdresse.replace("\\s".toRegex(), "")}/${datenbank}?"
//        val url = "jdbc:mysql://localhost:3306/kotlingruppe?"
//        val user = "root"
//        val password = "supergeheim"

        return DriverManager.getConnection(url, dbUsername.replace("\\s".toRegex(), ""), dbPassword.replace("\\s".toRegex(), ""))
    }


	@Throws(AuthenticationException::class)
    fun login(user: String) {
        this.user = user
        con = getConnection() // start new connection
        allSongs = allSongs()

        val query = "SELECT rolle FROM t_person WHERE gastname = ?"
        val statement = con.prepareStatement(query)
        statement.setString(1, user)
        val results = statement.executeQuery()

        isHost = if (results.next()) {
            val isAdmin = results.getBoolean("rolle")
            statement.close()
            isAdmin
        } else {
            statement.close()
			closeConnection()
            throw AuthenticationException("Unknown user!")
        }
    }
	
	@Throws(SQLException::class)
	fun register(user: String, title: String?){
		this.user = user
		isHost = false
		con = getConnection()
		
		allSongs = allSongs()
				
		val query = "REPLACE INTO t_person VALUES (?, ?, ?)"
		val statement = con.prepareStatement(query)
		statement.setString(1, user)
		statement.setBoolean(2, false)
		statement.setString(3, title)
		
		try{
			statement.execute()
		}catch(e: SQLException){
			closeConnection()
			throw e
		}finally{
			statement.close()
		}
	}

    private fun allSongs(): ArrayList<Song> {
        val query = "SELECT p_musiktitel_id, titelname, bandname, genre FROM t_musiktitel"
        val list: ArrayList<Song> = arrayListOf()
        val statement = con.createStatement()
        val results = statement.executeQuery(query)

        while (results.next()) {
            list.add(
                Song(
                    results.getInt("p_musiktitel_id"),
                    results.getString("titelname"),
                    results.getString("bandname"),
                    results.getString("genre")
                )
            )
        }
        statement.close()
        return list
    }


    fun addSong(title: String, band: String, genre: String): Song {
        val query = "INSERT INTO t_musiktitel(titelname, bandname, genre) VALUES (?, ?, ?)"
        val statement = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)
        statement.setString(1, title)
        statement.setString(2, band)
        statement.setString(3, genre)
        statement.execute()

        val keys = statement.generatedKeys
        keys.next()
        val id = keys.getInt(1)
        val newSong = Song(id, title, band, genre)
        allSongs.add(newSong)

        statement.close()
        return newSong
    }


    fun addVote(titleId: Int): Boolean {
        val query = "INSERT INTO t_person_musiktitel(f_gastname, f_musiktitel_id) VALUES (?, ?);"
        val statement = con.prepareStatement(query)
        statement.setString(1, user)
        statement.setInt(2, titleId)
        val success =  statement.execute()
        statement.close()
        return success
}


    fun getLastestVotes(): ArrayList<Song> {
        val query =
            "SELECT t1.f_musiktitel_id, COUNT(t1.f_musiktitel_id) AS anzahl " +
                    "FROM (SELECT t2.f_musiktitel_id FROM t_person_musiktitel AS t2 " +
                    "WHERE `f_gastname` = ?  ORDER By p_votetime DESC  LIMIT 5) AS t1 " +
                    "GROUP BY t1.f_musiktitel_id"

        val statement = con.prepareStatement(query)
        statement.setString(1, user)
        val results = statement.executeQuery()

        val changedSongs = arrayListOf<Song>()
        while (results.next()) {
            for (song in allSongs){
                if (song.id == results.getInt("t1.f_musiktitel_id")) {
                    song.votes = results.getInt("anzahl")
                    changedSongs.add(song)
                }else if(song.votes != 0 && !changedSongs.contains(song)){
					song.votes = 0
					changedSongs.add(song)
				}
            }
        }
        statement.close()
        return changedSongs
    }


    fun getPlaylist(): ArrayList<Song> {
        val con = getConnection()
        val query = "SELECT allSongs.*, COUNT(*) AS Anzahl " +
					"FROM (SELECT allVotes1.f_musiktitel_id " +
						"FROM t_person_musiktitel AS allVotes1 " +
						"LEFT JOIN t_person AS allPersons ON allVotes1.f_gastname = allPersons.gastname " +
						"WHERE allPersons.adelsgeschlecht IS NOT NULL " +
						"AND (SELECT COUNT(*) " +
							"FROM  t_person_musiktitel AS allVotes2 " +
							"WHERE allVotes2.f_gastname = allVotes1.f_gastname " +
							"AND allVotes2.p_votetime >= allVotes1.p_votetime) <= 5) AS allValidVotes " +
					"LEFT JOIN t_musiktitel AS allSongs ON allValidVotes.f_musiktitel_id = allSongs.p_musiktitel_id " +
					"GROUP BY allValidVotes.f_musiktitel_id " +
					"ORDER BY Anzahl DESC;"

        val playlist: ArrayList<Song> = arrayListOf()
        val statement = con.createStatement()
        val results = statement.executeQuery(query)

        while (results.next()) {
            playlist.add(
                Song(
                    results.getInt("p_musiktitel_id"),
                    results.getString("titelname"),
                    results.getString("bandname"),
                    results.getString("genre"),
                    results.getInt("anzahl")
                )
            )
        }
        statement.close()
        return playlist
    }

    fun updateList(): ArrayList<Song> {
        allSongs = allSongs()
        return allSongs
    }

    fun closeConnection() {
        con.close()
        user = ""
        isHost = false
    }
}