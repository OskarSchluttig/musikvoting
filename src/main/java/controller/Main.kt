package controller

import view.LoginFrame
import javax.swing.SwingUtilities


object Main {
    @JvmStatic
    fun main(args: Array<String>) {
        val url: String
        val db: String
        val username: String
        val password: String
        if (args.isNotEmpty()) {
            url = args[0].replace("\\s".toRegex(), "")
            db = args[1].replace("\\s".toRegex(), "")
            username = args[2].replace("\\s".toRegex(), "")
            password = args[3].replace("\\s".toRegex(), "")
        } else {
            url = "localhost:3306"
            db = "kotlingruppe"
            username = "musikvoter"
            password = "123"
        }
        val con = DBConnection(url, db, username, password)
        SwingUtilities.invokeLater { LoginFrame(con) }
    }
}
