package view;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.nio.charset.Charset;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import controller.DBConnection;
import controller.Song;

@SuppressWarnings("serial")
public class VotingFrame extends JFrame {
	private DBConnection con;
	private SongTableModel tableModel = new SongTableModel();
	
	public VotingFrame(DBConnection con) {
		super();
		this.con = con;
		initUI();
	}

	private void initUI() {
		setTitle("Musikvoting - " + this.con.getUser());
		setMinimumSize(new Dimension(500, 500));
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new GridBagLayout());

		GridBagConstraints cons = new GridBagConstraints();
		cons.anchor = GridBagConstraints.FIRST_LINE_START;
		cons.insets = new Insets(5, 5, 5, 5);
		cons.fill = GridBagConstraints.HORIZONTAL;
		cons.weightx = 1;

		PlaceholderTextField txtTitle = new PlaceholderTextField("Titel");
		add(txtTitle, cons);

		PlaceholderTextField txtInterpret = new PlaceholderTextField("Interpret");
		add(txtInterpret, cons);

		PlaceholderTextField txtGenre = new PlaceholderTextField("Genre(s)");
		add(txtGenre, cons);

		cons.weightx = 0;

		JButton btnAddSong = new JButton(new String("Hinzufügen".getBytes(Charset.forName("UTF-8"))));
		btnAddSong.addActionListener(e -> {
			try {
				tableModel.addRow(con.addSong(txtTitle.getText(), txtInterpret.getText(), txtGenre.getText()));
			}catch(Exception e1){
				JOptionPane.showMessageDialog(null, e1.getMessage());
			}
		});
		add(btnAddSong, cons);

		cons.anchor = GridBagConstraints.FIRST_LINE_END;

		JButton btnLogout = new JButton("Logout");
		btnLogout.addActionListener(e -> {
			con.closeConnection();
			setVisible(false);
			new LoginFrame(con);
			dispose();
		});
		add(btnLogout, cons);

		cons.gridx = 0;
		cons.gridy = 1;
		cons.gridwidth = 5;
		cons.fill = GridBagConstraints.BOTH;
		cons.anchor = GridBagConstraints.CENTER;
		cons.weightx = 1;
		cons.weighty = 1;
		
		JTable tblSongs = new JTable(tableModel);
		tblSongs.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		ListSelectionModel lsm = tblSongs.getSelectionModel();
		lsm.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(e.getValueIsAdjusting() || lsm.isSelectionEmpty()) {
					return;
				}
				Song s = tableModel.getRow(tblSongs.getSelectedRow());
				con.addVote(s.getId());
				tableModel.updateRows(con.getLastestVotes());
				lsm.clearSelection();
			}
		});
		add(new JScrollPane(tblSongs), cons);
		
		tableModel.addRows(con.getAllSongs());
		tableModel.updateRows(con.getLastestVotes());
		
		if(this.con.isHost()) {
			cons.gridx = 2;
			cons.gridy = 4;
			cons.weightx = 0;
			cons.weighty = 0;
			cons.fill = GridBagConstraints.NONE;
			cons.anchor = GridBagConstraints.LAST_LINE_END;
			JButton btnGeneratePlaylist = new JButton("Playlist erstellen");
			btnGeneratePlaylist.addActionListener(e -> {
				new PlaylistFrame(con.getPlaylist());
			});
			add(btnGeneratePlaylist, cons);
		}
		
		setVisible(true);
		setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
	}

}
