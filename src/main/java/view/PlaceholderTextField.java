package view;

import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JTextField;

/**
 * Taken from <a href="https://stackoverflow.com/a/16229082">StackOverflow</a>.
 * <br>
 * Licensed under <a href="https://creativecommons.org/licenses/by-sa/4.0/">BY-SA 4.0</a>
 * <p>
 * Modified by Oskar Schluttig <oskar-schluttig@gmx.net>
 */
@SuppressWarnings("serial")
public class PlaceholderTextField extends JTextField {
    private String placeholder;

    public PlaceholderTextField(final String pText) {
        super();
        this.placeholder = pText;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    @Override
    protected void paintComponent(final Graphics pG) {
        super.paintComponent(pG);

        if (placeholder == null || placeholder.length() == 0 || getText().length() > 0) {
            return;
        }

        final Graphics2D g = (Graphics2D) pG;
        g.setColor(getDisabledTextColor());
        g.drawString(placeholder, getInsets().left, pG.getFontMetrics()
            .getMaxAscent() + getInsets().top);
    }

    public void setPlaceholder(final String s) {
        placeholder = s;
    }

}