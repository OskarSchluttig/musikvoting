package view;

import java.awt.GridLayout;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import controller.Song;

@SuppressWarnings("serial")
public class PlaylistFrame extends JFrame{
	
	public PlaylistFrame(List<Song> songs) {
		super();
		setTitle("Musikvoting - Playlist");
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLayout(new GridLayout(1, 1));
		
		SongTableModel model = new SongTableModel();
		
		JTable table = new JTable(model);
		add(new JScrollPane(table));
		
		model.addRows(songs);
		
		setVisible(true);
		setSize(800, 600);
	}

}
