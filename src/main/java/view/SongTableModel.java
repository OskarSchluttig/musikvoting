package view;

import java.util.ArrayList;
import java.util.List;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import controller.Song;

public class SongTableModel implements TableModel {
	private List<Song> data = new ArrayList<>();
	private List<TableModelListener> listeners = new ArrayList<>();
	
	public void addRow(Song s) {
		TableModelEvent event = new TableModelEvent(this,
				this.data.size(),
				this.data.size(),
				TableModelEvent.ALL_COLUMNS,
				TableModelEvent.INSERT);
		this.data.add(s);
		for(TableModelListener l : this.listeners) {
			l.tableChanged(event);
		}
	}
	
	public Song getRow(int index) {
		return data.get(index);
	}
	
	public void updateRow(Song s) {
		for(int i = 0; i < data.size(); i++) {
			Song row = data.get(i);
			if(row.getId()==s.getId()) {
				row.setVotes(s.getVotes());
				notifyListeners(new TableModelEvent(this, i));
				return;
			}
		}
	}
	
	public void updateRows(List<Song> songs) {
		songs.forEach(s -> updateRow(s));
	}
	
	public void addRows(List<Song> s) {
		TableModelEvent event = new TableModelEvent(this,
				this.data.size(),
				this.data.size() + s.size() - 1,
				TableModelEvent.ALL_COLUMNS,
				TableModelEvent.INSERT);
		this.data.addAll(s);
		notifyListeners(event);
	}
	
	public void removeAllRows() {
		TableModelEvent event = new TableModelEvent(this,
				0,
				this.data.size()-1,
				TableModelEvent.ALL_COLUMNS,
				TableModelEvent.DELETE);
		
		this.data.clear();
		notifyListeners(event);
	}

	@Override
	public int getRowCount() {
		return data.size();
	}

	@Override
	public int getColumnCount() {
		return 4;
	}

	@Override
	public String getColumnName(int columnIndex) {
		switch(columnIndex) {
		case 0:
			return "Titel";
		case 1:
			return "Interpret";
		case 2:
			return "Genre(s)";
		case 3:
			return "Votes";
		default:
			return null;
		}
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch(columnIndex) {
		case 0:
			return String.class;
		case 1:
			return String.class;
		case 2:
			return String.class;
		case 3:
			return int.class;
		default:
			return null;
		}
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Song song = data.get(rowIndex);
		switch(columnIndex) {
		case 0:
			return song.getTitle();
		case 1:
			return song.getBand();
		case 2:
			return song.getGenre();
		case 3:
			return song.getVotes();
		default:
			return null;
		}
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		// isn't editable, since there are no setters for song attributes
	}

	@Override
	public void addTableModelListener(TableModelListener l) {
		this.listeners.add(l);
	}

	@Override
	public void removeTableModelListener(TableModelListener l) {
		this.listeners.remove(l);
	}
	
	public void notifyListeners(TableModelEvent e) {
		for(TableModelListener l : listeners) {
			l.tableChanged(e);
		}
	}

}
