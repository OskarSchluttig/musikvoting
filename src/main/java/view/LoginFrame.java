package view;

import java.awt.Dimension;
import java.sql.SQLException;

import javax.naming.AuthenticationException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import controller.DBConnection;

@SuppressWarnings("serial")
public class LoginFrame extends JFrame {
	private JTextField txtName;
	private JTextField txtAdelsgeschlecht;
	private JButton btnLogin;
	private JButton btnRegister;
	private final DBConnection con;
	
	public LoginFrame(DBConnection con) {
		super();
		this.con = con;
		initUI();
	}
	
	private void initUI() {
		setTitle("Musikvoting - Login");
		setPreferredSize(new Dimension(390,190));
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
		setLayout(null);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setBounds(10, 10, 85, 30);
		add(lblName);

		txtName = new JTextField();
		txtName.setBounds(195, 10, 175, 30);
		add(txtName);

		btnRegister = new JButton("Register");
		btnRegister.setBounds(270, 120, 100, 30);
		add(btnRegister);

		btnRegister.addActionListener(e -> {
			register();
		});

		btnLogin = new JButton("Login");
		btnLogin.setBounds(270, 80, 100, 30);
		add(btnLogin);
		
		btnLogin.addActionListener(e -> {
			login();
		});
		
		setVisible(true);
		pack();
	}
	
	private void login() {
		String name = txtName.getText();
		if(name.isBlank()) {
			JOptionPane.showMessageDialog(this, "Bitte geben Sie Ihren Namen ein.");
			return;
		}
		try {
			con.login(name);
			new VotingFrame(con);
			dispose();
		}catch(AuthenticationException e) {
			register();
		}catch(Exception e) {
			JOptionPane.showMessageDialog(this, e);
		}
	}
	private void register(){
		setTitle("Musikvoting - Register");
		setPreferredSize(new Dimension(390,190));
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
		setLayout(null);
		
		btnRegister.setVisible(false);

		JLabel lblAdelsgeschlecht = new JLabel("Adelsgeschlecht:");
		lblAdelsgeschlecht.setBounds(10, 50, 175, 30);
		add(lblAdelsgeschlecht);

		txtAdelsgeschlecht = new JTextField();
		txtAdelsgeschlecht.setBounds(195, 50, 175, 30);
		add(txtAdelsgeschlecht);

		btnLogin.removeActionListener(btnLogin.getActionListeners()[0]);
		btnLogin.addActionListener(e -> {
			register_login();
		});
		repaint();
	}
	
	private void register_login(){
		String name = txtName.getText();
		if(name.isBlank()) {
			JOptionPane.showMessageDialog(this, "Bitte geben Sie Ihren Namen ein.");
			return;
		}
		String title = txtAdelsgeschlecht.getText();
		if(title.isBlank()) {
			title = null;
		}
		try {
			con.register(name, title);
			new VotingFrame(con);
			dispose();
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(this, e);
		}
	}

	
}